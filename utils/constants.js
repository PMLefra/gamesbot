const userRegex = /<@!?(\d+)>/;

const emojiNumbers = [":zero:", ":one:", ":two:", ":three:", ":four:", ":five:", ":six:", ":seven:", ":eight:", ":nine:"];

const COLORS = {
  info: "#eac407",
  warning: "#c78011",
  stop: "#be3e3e",
  error: "#9c1515",
  success: "#10a810",
  status: "#2b86af",
  statusDark: "#436586",
  result: "#5e4f8a"
}

const ERRORS = {
  nArgs: n => `Un minimum de ${n} argument${n > 1 ? 's' : ''} est requis`,
};

module.exports = {
  userRegex,
  COLORS,
  ERRORS,
  emojiNumbers
};