const { Command } = require('../utils/classes');
const Logger = require("js-logger");
const { statusEmbed, capitalize, saveWords, frsort } = require('../utils/functions');
const { COLORS, ERRORS } = require('../utils/constants');
const Discord = require('discord.js');
const https = require('https');

let word = new Command('word', "permet d'interagir avec le dictionnaire du bot", Command.EVERYONE, ["words", "mot", "mots", "dictionary", "dictionnaire"]);
word.subcommands["random"] = new Command('random', "donne un mot aléatoire de la langue française", Command.EVERYONE, ["aléatoire"]);
word.subcommands["def"] = new Command('def', "donne la définition d'un mot", Command.EVERYONE, ["déf", "definition", "définition"], "<mot>");
word.subcommands["propose"] = new Command('propose', "ajoute un mot à la liste des mots proposés pour être ajoutés au dictionnaire", Command.EVERYONE, [], "<mot>");
word.subcommands["show-proposed"] = new Command('show-proposed', "affiche la liste des mots proposés", Command.MODS_ONLY, ["sp"]);
word.subcommands["remove-proposed"] = new Command('remove-proposed', "retire un mot de la liste des mots proposés", Command.MODS_ONLY, ["rp"], "<mot>");
word.subcommands["add"] = new Command('add', "ajoute un ou plusieurs mots au dictionnaire", Command.MODS_ONLY, [], "<mot 1> <mot 2> ...");
word.subcommands["remove"] = new Command('remove', "retire un ou plusieurs mots du dictionnaire", Command.MODS_ONLY, [], "<mot 1> <mot 2> ...");
word.subcommands["get"] = new Command('get', "convertit une liste de mots ou d'indices en indices ou mots correspondants en mémoire", Command.MODS_ONLY, ["get"], "<mot/indice 1> <mot/indice 2> ...");
word.subcommands["exists"] = new Command('exists', "permet de savoir si un mot est présent dans le dictionnaire ou non", Command.EVERYONE, ["exist", "existe"], "<mot>");

word.run = async function(msg) {
  msg.content = '$help word';
  global.registry.commands['help'].exec(msg);
}

word.subcommands["random"].run = async function(msg) {
  let index = Math.floor(Math.random() * global.registry.words.length);
  await msg.channel.send(statusEmbed(capitalize(global.registry.words[index])));
}

word.subcommands["def"].run = async function(msg) {
  let parts = msg.content.split(/\s+/);
  if (parts.length < 2)
    return msg.channel.send(statusEmbed(ERRORS.nArgs(1), COLORS.error));

  parts[1] = parts[1].slice(0, 100);

  https.get(`https://api.dictionaryapi.dev/api/v2/entries/fr/${parts[1]}`, res => {
    if (res.statusCode == 404)
      return msg.channel.send(statusEmbed("Cette définition n'a pas été trouvée 🙄",
        COLORS.error,
        "",
        `Cependant, tu peux essayer [avec le wiktionnaire](https://fr.wiktionary.org/wiki/${parts[1]}),\nou sinon [avec Google](https://www.google.com/search?q=d%C3%A9finition+${parts[1]}).`));

    if (res.statusCode != 200)
      return msg.channel.send(statusEmbed("Y'a un problème avec l'API où je récupère les définitions, réessaye plus tard", COLORS.error, "https://emojipedia-us.s3.dualstack.us-west-1.amazonaws.com/thumbs/240/twitter/282/confused-face_1f615.png"));

    let data = '';

    res.on('data', chnk => {
      data += chnk;
    });

    res.on('end', () => {
      let jsonData = JSON.parse(data);

      let embed = new Discord.EmbedBuilder()
        .setColor(COLORS.result)
        .setTitle(`Définition de \`${parts[1]}\``)
        .setThumbnail("https://emojipedia-us.s3.dualstack.us-west-1.amazonaws.com/thumbs/120/twitter/281/books_1f4da.png");

      for (let word of jsonData) {
        for (let meaning of word.meanings) {
          let title = `${word.word} : *${meaning.partOfSpeech}*`;
          let def = '';

          for (let definition of meaning.definitions) {
            def += `\n➣ ${capitalize(definition.definition)}`;
            if (definition.example) def += `\n\u00a0\u00a0\u00a0Exemple : *${definition.example}*`;
          }

          embed.addFields({ name: title, value: def });
        }
      }

      return msg.channel.send(embed);
    });
  }).on("error", e => {
    logger.log(e.toString(), "https-get", Logger.INTERNAL, 3);
    msg.channel.send(statusEmbed("Une erreur est survenue lors de la recherche de définition",
      COLORS.error,
      "https://emojipedia-us.s3.dualstack.us-west-1.amazonaws.com/thumbs/240/twitter/282/fire_1f525.png",
      "Réessaye dans quelques secondes si tu y tiens vraiment !"));
  });
}

word.subcommands["propose"].run = async function (msg) {
  let parts = msg.content.split(/\s+/);

  if (parts.length < 2)
    return msg.channel.send(statusEmbed(ERRORS.nArgs(1), COLORS.error));

  if (parts[1].length > 100)
    return msg.channel.send(statusEmbed("Ton mot est trop long", COLORS.error));

  global.data.proposedwords.push(parts[1]);
  return msg.channel.send(statusEmbed(`Le mot \`${parts[1]}\` a bien été ajouté à la liste des propositions`, COLORS.success));
}

word.subcommands["show-proposed"].run = async function (msg) {
  let fstr = JSON.stringify(global.data.proposedwords, null, 2);
  fstr = '```js\n' + fstr + '```';

  return msg.channel.send(statusEmbed("Voici la liste des mots proposés pour être ajoutés au dictionnaire", COLORS.result, "", fstr));
}

word.subcommands["remove-proposed"].run = async function (msg) {
  let parts = msg.content.split(/\s+/);

  if (parts.length < 2)
    return msg.channel.send(statusEmbed(ERRORS.nArgs(1), COLORS.error));

  let index = global.data.proposedwords.indexOf(parts[1]);
  if (index == -1)
    return msg.channel.send(statusEmbed("Ce mot n'a pas été proposé", COLORS.error));

  global.data.proposedwords.splice(index, 1);
  return msg.channel.send(statusEmbed(`Le mot \`${parts[1]}\` a bien été supprimé de la liste des propositions`, COLORS.success));
}

word.subcommands["add"].run = async function (msg) {
  let parts = msg.content.split(/\s+/);

  if (parts.length < 2)
    return msg.channel.send(statusEmbed(ERRORS.nArgs(1), COLORS.error));

  let nAdded = 0;
  for (let i = 1; i < parts.length; i++) {
    if (parts[i].length > 100) {
      await msg.channel.send(statusEmbed(`Le mot \`${parts[i]}\` est trop long`, COLORS.error));
      continue;
    }

    let indexProposed = global.data.proposedwords.indexOf(parts[i]);
    if (indexProposed != -1)
      global.data.proposedwords.splice(indexProposed, 1);

    let indexRegistry = global.registry.words.indexOf(parts[i]);
    if (indexRegistry != -1) {
      await msg.channel.send(statusEmbed(`Le mot \`${parts[i]}\` est déjà dans le dictionnaire`, COLORS.error));
      continue;
    }

    global.registry.words.push(parts[i]);
    nAdded++;
    await msg.channel.send(statusEmbed(`Le mot \`${parts[i]}\` a été ajouté au dictionnaire`, COLORS.success));
  }

  if (nAdded > 0) {
    global.registry.words.sort(frsort);
    await saveWords();
  }
}

word.subcommands["remove"].run = async function (msg) {
  let parts = msg.content.split(/\s+/);

  if (parts.length < 2)
    return msg.channel.send(statusEmbed(ERRORS.nArgs(1), COLORS.error));

  let nRemoved = 0;
  for (let i = 1; i < parts.length; i++) {
    let indexRegistry = global.registry.words.indexOf(parts[i]);
    if (indexRegistry == -1) {
      await msg.channel.send(statusEmbed(`Le mot \`${parts[i]}\` n'est pas présent dans le dictionnaire`, COLORS.error));
      continue;
    }

    global.registry.words.splice(indexRegistry, 1);
    nRemoved++;
    await msg.channel.send(statusEmbed(`Le mot \`${parts[i]}\` a été supprimé du dictionnaire`, COLORS.success));
  }

  if (nRemoved > 0) {
    global.registry.words.sort(frsort);
    await saveWords();
  }
}

word.subcommands["get"].run = async function (msg) {
  let parts = msg.content.split(/\s+/);

  if (parts.length < 2)
    return msg.channel.send(statusEmbed(ERRORS.nArgs(1), COLORS.error));

  for (let i = 1; i < parts.length; i++) {
    let part = parts[i];

    // Cas d'un indice donné
    let searchIndex = parseInt(part);
    if (!isNaN(searchIndex)) {
      if (searchIndex < 0 || searchIndex >= global.registry.words.length) {
        await msg.channel.send(statusEmbed("L'indice donné est invalide (trop grand ou trop petit)", COLORS.error));
        continue;
      }

      await msg.channel.send(statusEmbed(`L'indice \`${searchIndex}\` correspond au mot \`${global.registry.words[searchIndex]}\``, COLORS.info));
      continue;
    }

    // Cas d'un mot donné
    part = part.toLowerCase();
    let index = global.registry.words.indexOf(part);
    if (index == -1) {
      await msg.channel.send(statusEmbed(`Le mot \`${part}\` n'est pas présent dans le dictionnaire`, COLORS.error));
      continue;
    }

    await msg.channel.send(statusEmbed(`Le mot \`${part}\` a pour indice \`${index}\``, COLORS.info));
  }
}

word.subcommands["exists"].run = async function(msg) {
  let parts = msg.content.split(/\s+/);

  if (parts.length < 2)
    return msg.channel.send(statusEmbed(ERRORS.nArgs(1), COLORS.error));

  parts[1] = parts[1].toLowerCase();

  if (global.registry.words.includes(parts[1]))
    return msg.channel.send(statusEmbed(`Le mot \`${parts[1]}\` est bien présent dans le dictionnaire`, COLORS.success));
  else return msg.channel.send(statusEmbed(`Le mot \`${parts[1]}\` n'est pas présent dans le dictionnaire`, COLORS.error));
}

module.exports = word;