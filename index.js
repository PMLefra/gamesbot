const { Client, GatewayIntentBits, ActivityType } = require("discord.js");
const fs = require("fs");
const Logger = require("js-logger");
const { saveData, sendDM } = require('./utils/functions');
//const isASlotAnyGood = require('./utils/rdv-mairie');

process.chdir(__dirname);

global.data = require("./data.json");
global.client = new Client({
  intents: [
    GatewayIntentBits.Guilds,
    GatewayIntentBits.GuildMessages,
    GatewayIntentBits.GuildMessageReactions,
    GatewayIntentBits.GuildEmojisAndStickers,
    GatewayIntentBits.GuildIntegrations,
    GatewayIntentBits.DirectMessages,
    GatewayIntentBits.DirectMessageReactions,
    GatewayIntentBits.MessageContent
  ]
});
global.logger = new Logger('./logs');
global.logInternalUser = Logger.INTERNAL;

require('./registry');

client.on("ready", async () => {
  logger.log(`Discord bot user '${client.user.tag}' connected.`, 'bot', Logger.INTERNAL, 1);
  client.user.setActivity(`${global.registry.prefix}help`, { type: ActivityType.Watching });
});

client.on("messageCreate", msg => {
  if (global.registry.channels.indexOf(msg.channel.id) == -1) return;
  if (msg.author.id == client.user.id) return;

  let msgTxt = msg.content.trim();

  if (/[Pp]oulpe/.test(msgTxt)) msg.react('🦑').catch();
  if (/[Cc]hien/.test(msgTxt)) msg.react('🐶').catch();
  if (/[Pp]ingouin/.test(msgTxt)) msg.react('🐧').catch();
  if (/[Mm]anchot/.test(msgTxt)) msg.react('🐧').catch();
  if (/[Cc]hat/.test(msgTxt)) msg.react('🐈').catch();
  if (/[Cc]anard/.test(msgTxt)) msg.react('🦆').catch();

  // Gère les commandes
  let content = msgTxt.split(/\s+/);
  if (content[0].length > 0 && content[0][0] == global.registry.prefix) {
    content[0] = content[0].replace(global.registry.prefix, '').toLowerCase();
    if (global.registry.commands.hasOwnProperty(content[0])) {
      global.registry.commands[content[0]].exec(msg);
    }
  }
});

// Auto-saves data every 5min
setInterval(async function () {
  await saveData();
  //const slotFree = await isASlotAnyGood();
  //logger.log(`Passport slot free? ${slotFree}`, 'rdv-mairie', Logger.INTERNAL, 0);
  
  //if (slotFree)
  //  sendDM("217613526598942722", "Incroyable !! <@!217613526598942722> !! Un créneau est libre !!");
}, 300000);

client.login(fs.readFileSync("bot.key").toString())
  .catch(err => {
    throw err;
  });
