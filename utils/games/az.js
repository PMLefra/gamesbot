const { capitalize, statusEmbed, createUser, addMoney } = require("../functions");
const { COLORS } = require('../constants');
const Logger = require("js-logger");

class AzGame {
  static games = {};

  constructor(wordIndex, channel) {
    this.wordIndex = wordIndex == -1 ? Math.floor(Math.random() * global.registry.words.length) : wordIndex;
    this.aIndex = 0;
    this.bIndex = global.registry.words.length - 1;

    this.channel = channel;
    this.status = "not-started";
    this.tries = 0;
    this.isCustom = false;

    this.messageListener = async msg => {
      if (msg.channel.id != this.channel.id) return;
      if (msg.author.id == global.client.user.id) return;
      if (this.status != "started") return;

      await this.guess(msg);
    }

    global.client.on("messageCreate", this.messageListener);
  }

  setWord(newWord) {
    let newIndex = global.registry.words.indexOf(newWord.toLowerCase());
    if (newIndex != -1) this.wordIndex = newIndex;
    if (this.aIndex >= this.wordIndex) this.aIndex = 0;
    if (this.bIndex <= this.wordIndex) this.bIndex = global.registry.words.length - 1;
  }

  start() {
    this.status = "started";
    if (!AzGame.games.hasOwnProperty(this.channel.id))
      AzGame.games[this.channel.id] = this;

    this.channel.send(statusEmbed(
      "C'est parti !",
      COLORS.info,
      "https://emojipedia-us.s3.dualstack.us-west-1.amazonaws.com/thumbs/240/google/241/chequered-flag_1f3c1.png",
      "Une partie de A-Zythum vient de démarrer.\nTrouve le mot auquel je pense en proposant des mots entre les deux bornes."
    ));

    this.statusMessage();
  }

  pause() {
    this.status = "paused";
  }

  resume() {
    this.status = "started";
  }

  statusMessage() {
    if (this.status == "paused") this.channel.send(statusEmbed(
      "La partie est en pause",
      COLORS.statusDark,
      "https://emojipedia-us.s3.dualstack.us-west-1.amazonaws.com/thumbs/240/twitter/281/hourglass-done_231b.png",
      ""
    ));
    else if (this.status == "waiting") this.channel.send(statusEmbed(
      "🤷‍♂️ Une partie est en cours de préparation",
      COLORS.warning
    ));

    if (this.status != "started") return;

    let content = capitalize(global.registry.words[this.aIndex]) +
      ' ➣ ' +
      capitalize(global.registry.words[this.bIndex]);
    let remaining = "Mots dans l'intervalle : " +
      (this.bIndex - this.aIndex - 1);

    this.channel.send(statusEmbed(
      content,
      COLORS.status,
      "", "",
      remaining
    ));
  }

  async guess(msg) {
    if (this.status != "started") return;
    if (msg.content.length > 1 && msg.content[0] == global.registry.prefix) return;

    let msgTxt = msg.content.trim();
    if (msgTxt.split(/\s+/).length != 1) return;
    msgTxt = msgTxt.toLowerCase();

    let index = global.registry.words.indexOf(msgTxt);

    this.tries++;

    if (index == -1) msg.react('❌').catch();
    else if (index < this.aIndex) msg.react('➡️').catch();
    else if (index > this.bIndex) msg.react('⬅️').catch();
    else {
      if (index < this.wordIndex) {
        this.aIndex = index;
        this.statusMessage();
      } else if (index > this.wordIndex) {
        this.bIndex = index;
        this.statusMessage();
      } else await this.win(msg);
    }
  }

  async win(msg) {
    let totalTries = this.tries;
    let responseContent = `${msg.author.username} a trouvé le bon mot après ${totalTries} propositions, qui était **${global.registry.words[this.wordIndex]}**.`;

    if (!this.isCustom) {
      await createUser(msg).catch(e => logger.log(e.toString(), "create-user", Logger.INTERNAL, 3));
      addMoney(msg.author.id, 10);
      responseContent += `\nIncroyable ! ${global.registry.moneySymbol} 10 sont apparus sur son compte, on l'applaudit tous, s'il vous plaît.`;
    } else
      responseContent += `\nPuisqu'il s'agissait d'une partie custom, iel ne gagne aucun ${global.registry.moneySymbol}, mais on l'applaudit tous quand même, s'il vous plaît.`;

    this.channel.send(statusEmbed(
      "Félicitations !",
      COLORS.success,
      "https://emojipedia-us.s3.dualstack.us-west-1.amazonaws.com/thumbs/240/google/241/party-popper_1f389.png",
      responseContent
    ));

    this.end();
  }

  end() {
    this.status = "ended";
    global.client.removeListener("messageCreate", this.messageListener);
    delete AzGame.games[this.channel.id];
  }
}

module.exports = AzGame;