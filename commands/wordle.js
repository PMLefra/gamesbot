const { Command } = require('../utils/classes');
const { statusEmbed } = require('../utils/functions');
const { COLORS } = require('../utils/constants');
const WordleGame = require('../utils/games/wordle');

let wordle = new Command('wordle', 'commande générale permettant de jouer à Wordle', 0, ['lemot']);
wordle.dispBaseCommandInSubcommandMenu = true;
wordle.baseCommandDescription = "démarre une partie avec un mot aléatoire, relance une partie en pause ou affiche l'état actuel de la partie";

wordle.subcommands["custom"] = new Command('custom', 'lance une partie avec un mot custom qui sera demandé en MP');
wordle.subcommands["pause"] = new Command('pause', 'met la partie en cours en pause');
wordle.subcommands["end"] = new Command('end', 'arrête la partie');

wordle.run = async function (msg) {
  if (msg.channel.id in WordleGame.games) {
    if (WordleGame.games[msg.channel.id].status == "paused") {
      await msg.channel.send(statusEmbed(
        "➣ La partie de Wordle a redémarré",
        COLORS.info
      ));
      WordleGame.games[msg.channel.id].resume();
    }

    WordleGame.games[msg.channel.id].statusMessage();
  }
  else {
    WordleGame.games[msg.channel.id] = new WordleGame(-1, msg.channel);
    WordleGame.games[msg.channel.id].start();
  }
};

wordle.subcommands["custom"].run = async function (msg) {
  if (msg.channel.id in WordleGame.games) {
    await msg.channel.send(statusEmbed(
      "Il y a déjà une partie de Wordle en cours !",
      COLORS.error
    ));
  } else {
    let newGame = new WordleGame(-1, msg.channel);
    newGame.isCustom = true;
    WordleGame.games[msg.channel.id] = newGame;
    WordleGame.games[msg.channel.id].status = "waiting";

    msg.author.createDM()
      .then(chan => {
        chan.send("Envoie ton mot pour le faire deviner aux autres zigotos dans une nouvelle partie de Wordle endiablée.\nAttention, il doit contenir exactement **5 lettres** et **aucun accent ou diacritique**.");

        let filter = m => m.author.id == msg.author.id;
        let coll = chan.createMessageCollector(filter);

        coll.on('collect', m => {
          let mTxt = m.content.trim().toLowerCase();
          let mContent = mTxt.split(/\s+/);
          if (mContent.length == 1 && mTxt.length == 5 && WordleGame.dictionary.indexOf(mContent[0]) != -1) {
            newGame.setWord(mContent[0]);
            newGame.start();
            chan.send("Parfait, la partie commence !");
          } else {
            chan.send("Ce n'est pas un mot valide. Retape la commande dans le salon où tu veux jouer STP.");
            newGame.end();
          }
          coll.stop();
        });
      })
      .catch(err => {
        logger.log(`Erreur lors de la création d'un chan DM pour ${msg.author.tag} : ${err.message}`, 'bot', logInternalUser, 3);
      });
  }
};

wordle.subcommands["pause"].run = async function (msg) {
  if (msg.channel.id in WordleGame.games) {
    if (WordleGame.games[msg.channel.id].status == "started") {
      WordleGame.games[msg.channel.id].pause();
      await msg.channel.send(statusEmbed(
        "⨯ Partie de Wordle mise en pause",
        COLORS.stop
      ));
    } else if (WordleGame.games[msg.channel.id].status == "paused") {
      await msg.channel.send(statusEmbed(
        "La partie est déjà en pause !",
        COLORS.error
      ));
    } else if (WordleGame.games[msg.channel.id].status == "waiting") {
      await msg.channel.send(statusEmbed(
        "La partie est en préparation !",
        COLORS.error
      ));
    }
  } else {
    await msg.channel.send(statusEmbed(
      "Il n'y a aucune partie de Wordle en cours !",
      COLORS.error
    ));
  }
};

wordle.subcommands["end"].run = async function (msg) {
  if (msg.channel.id in WordleGame.games) {
    if (WordleGame.games[msg.channel.id].status == "waiting") {
      await msg.channel.send(statusEmbed(
        "La partie en cours de préparation a été arrêtée",
        COLORS.stop,
        "https://emojipedia-us.s3.dualstack.us-west-1.amazonaws.com/thumbs/240/google/241/stop-sign_1f6d1.png",
        `La partie de Wordle qui était en cours de préparation a été arrêtée par ${msg.author.username}.`
      ));
    } else {
      await msg.channel.send(statusEmbed(
        "La partie est terminée",
        COLORS.stop,
        "https://emojipedia-us.s3.dualstack.us-west-1.amazonaws.com/thumbs/240/google/241/stop-sign_1f6d1.png",
        `La partie de Wordle qui était en cours a été arrêtée par ${msg.author.username}.
Il y avait jusque là ${WordleGame.games[msg.channel.id].tries} mot(s) proposé(s).
La mot était **${WordleGame.games[msg.channel.id].word.toUpperCase()}.**`
      ));
    }

    WordleGame.games[msg.channel.id].end();
  } else {
    await msg.channel.send(statusEmbed(
      "Il n'y a aucune partie de Wordle en cours !",
      COLORS.error
    ));
  }
};

module.exports = wordle;