const { Command } = require('../utils/classes');
const Logger = require("js-logger");
const { statusEmbed } = require('../utils/functions');
const { COLORS, ERRORS } = require('../utils/constants');
const fs = require('fs');

const reload = new Command('reload', 'charge ou recharge une commande', Command.OWNER_ONLY, ['load', 'charger', 'recharger'], '<command 1> <command 2> ...');
reload.subcommands["utils"] = new Command('utils', 'recharge un module utils', Command.OWNER_ONLY, [], '<utils 1> <utils 2> ...');

reload.run = async function(msg) {
  const args = msg.content.split(/\s+/);
  if (args.length < 2)
    return msg.channel.send(statusEmbed(ERRORS.nArgs(1), COLORS.error));

  for (let i = 1; i < args.length; i++) {
    try {
      const cmdName = args[i];

      let cmdFiles = fs.readdirSync('./commands').map(el => el.replace('.js', ''));
      if (cmdFiles.includes(cmdName)) {
        const path = './' + cmdName;
        delete require.cache[require.resolve(path)];
        const cmd = require(path);
        const names = [cmd.key, ...cmd.aliases];

        for (let name of names)
          global.registry.commands[name] = cmd;

        logger.log(`Command '${cmdName}' (re)loaded`, "reload", Logger.INTERNAL, 1);
        await msg.channel.send(statusEmbed(`La commande \`${global.registry.prefix}${cmdName}\` a été (re)chargée`, COLORS.success));
      } else await msg.channel.send(statusEmbed(`La commande \`${global.registry.prefix}${cmdName}\` n'existe pas...`, COLORS.error));
    } catch (e) {
      logger.log(e.toString(), "reload", Logger.INTERNAL, 3);
      await msg.channel.send(statusEmbed(`Quelque chose s'est mal passé pendant le (re)load de \`${global.registry.prefix}${args[i]}\`, regarde les logs`,
        COLORS.error,
        "https://emojipedia-us.s3.dualstack.us-west-1.amazonaws.com/thumbs/240/twitter/282/fire_1f525.png"));
    }
  }
}

reload.subcommands["utils"].run = async function(msg) {
  const args = msg.content.split(/\s+/);
  if (args.length < 2)
    return msg.channel.send(statusEmbed(ERRORS.nArgs(1), COLORS.error));

  for (let i = 1; i < args.length; i++) {
    try {
      const utilName = args[i];
      const path = require.resolve('../utils/' + utilName);
      const cachedUtil = require.cache[require.resolve(path)];

      // If the util is a game class and has active games, we end them all
      if (cachedUtil.exports instanceof Function && cachedUtil.exports.hasOwnProperty("games"))
          for (let chanId in cachedUtil.exports.games)
            cachedUtil.exports.games[chanId]?.end();

      delete require.cache[path];

      logger.log(`Utils module '${path}' reloaded`, "reload", Logger.INTERNAL, 1);
      await msg.channel.send(statusEmbed(`Le module utils \`${utilName}\` a été rechargé`, COLORS.success));
    } catch (e) {
      logger.log(e.toString(), "reload-utils", Logger.INTERNAL, 3);
      await msg.channel.send(statusEmbed(`Quelque chose s'est mal passé pendant le reload de \`${args[i]}\`, regarde les logs`,
        COLORS.error,
        "https://emojipedia-us.s3.dualstack.us-west-1.amazonaws.com/thumbs/240/twitter/282/fire_1f525.png"));
    }
  }
}

module.exports = reload;