let prefix = (global.registry && global.registry.prefix) ?
  global.registry.prefix :
  require('../config').prefix;

class Command {
  constructor(key = "", description = "", privilege = Command.EVERYONE, aliases = [], usage = "", channels = Command.ALL_CHANNELS, subcommands = {}, defaultData = {}) {
    if (!key) throw 'command key required';
    this.key = key;
    this.description = description;
    this.privilege = privilege;
    this.aliases = aliases;
    this.usage = usage;
    this.subcommands = subcommands;
    this.data = defaultData;
    this.channels = channels;
    this._help = "";

    // Display or not the subcommands usage in the help menu
    this.dispSubcommandUsage = true;
    /* Display or not the current command description and usage in the subcommand
    section of its help menu */
    this.dispBaseCommandInSubcommandMenu = false;
    /* Custom base command description if it is located in the subcommand section
    of its help menu (if the previous var is true) */
    this.baseCommandDescription = "";
  }

  set help(txt) {
    if (this._help.length != 0) this._help += '\n';
    this._help += txt;
  }

  get help() {
    return this._help;
  }

  setData(key, val) {
    this.data[key] = val;
  }

  userAllowed(user) {
    return (global.registry.owner == user.id
      || this.privilege == Command.MODS_ONLY && global.registry.moderators.includes(user.id)
      || this.privilege == Command.EVERYONE);
  }

  channelAllowed(channel) {
    return (this.channels === Command.ALL_CHANNELS
      || this.channels.includes(channel.id));
  }

  async exec(msg) {
    let parts = msg.content.split(/\s+/);
    parts[0] = parts[0].replace(prefix, '').toLowerCase();
    if (parts[0] != this.key && !this.aliases.includes(parts[0])) return;

    if (this.privilege > 1 && msg.author.id != global.registry.owner)
      return msg.reply("seul le créateur du bot peut utiliser cette commande.");
    else if (this.privilege == 1 && !global.registry.moderators.includes(msg.author.id))
      return msg.reply("tu n'as pas les droits pour cette commande, filou va !");
    else if (!this.channelAllowed(msg.channel))
      return msg.reply("cette commande est désactivée dans ce salon.");

    for (let subcommandName in this.subcommands) {
      if (parts[1] == subcommandName || this.subcommands[subcommandName].aliases.includes(parts[1])) {
        msg.content = prefix + parts.slice(1, parts.length).join(' ');
        return this.subcommands[subcommandName].exec(msg);
      }
    }

    return this.run(msg);
  }

  async run(msg) {}
}

Command.EVERYONE = 0;
Command.MODS_ONLY = 1;
Command.OWNER_ONLY = 2;
Command.ALL_CHANNELS = 0;
Command.NO_CHANNELS = [];

module.exports = { Command };
