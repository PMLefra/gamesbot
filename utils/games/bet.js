const Discord = require('discord.js');
const { roundMoney, statusEmbed, addMoney } = require('../functions');
const { COLORS, emojiNumbers } = require('../constants');
const ms = global.registry.moneySymbol;

class Bet {
  constructor(name, outcomes, state = Bet.CLOSED, bets = [], id = Date.now().toString()) {
    this.name = name;
    this.outcomes = outcomes;
    this.state = state
    // Format : [userID, outcome, amount]
    this.bets = bets;
    this.id = id;
  }

  bet(userID, chosenOutcome, amount) {
    const outcome = this.getOutcome(chosenOutcome);
    if (outcome === null) return;

    amount = Number(amount);
    if (isNaN(amount) || amount < 0 || amount > global.data.users[userID].money) return;

    const existingBet = this.bets.find(bet => bet[0] === userID)

    if (existingBet !== undefined) {
      if (existingBet[1] !== outcome) return null;
      for (let i = 0; i < this.bets.length; i++) {
        if (this.bets[i][0] === userID) this.bets[i][2] += amount;
      }
    } else {
      this.bets.push([userID, outcome, amount]);
    }
    global.data.users[userID].money -= amount;
    global.data.users[userID].money = roundMoney(global.data.users[userID].money);
  }

  open() {
    this.state = Bet.OPEN;
  }

  close() {
    this.state = Bet.CLOSED;
  }

  delete() {
    this.state = Bet.DELETED;

    for (let bet of this.bets) {
      global.data.users[bet[0]].money += bet[2];
      global.data.users[bet[0]].money = roundMoney(global.data.users[bet[0]].money);
    }
  }

  getOutcome(outcome) {
    let index = Number(outcome);
    if (isNaN(index)) index = this.outcomes.indexOf(outcome);
    if (index < 0 || index >= this.outcomes.length) return null;
    return index;
  }

  async end(winningOutcome) {
    const outcome = this.getOutcome(winningOutcome);
    if (outcome === null) return null;
    // Redistribution des gains
    const totalForWinningOutcome = this.totalForOutcome(outcome);
    for (const bet of this.bets) {
      const user = await client.users.fetch(bet[0], { cache: false });
      const chosenOutcome = bet[1];
      if (chosenOutcome === outcome) {
        const percentage = bet[2] / totalForWinningOutcome;
        const gains = percentage * this.total;
        addMoney(bet[0], gains);

        logger.log(`${global.data.users[bet[0]].username} won ${ms} ${gains}`, 'bot', { tag: global.data.users[bet[0]].username }, 1);
        await user.send(statusEmbed(`Résultats du pari "${this.name}"`, COLORS.success, "https://emojipedia-us.s3.dualstack.us-west-1.amazonaws.com/thumbs/120/twitter/281/trophy_1f3c6.png", `Félicitations ! Ton pari sur l'issue **${this.outcomes[chosenOutcome]}** t'a rapporté ${ms} ${gains} !`));
      } else {
        logger.log(`${global.data.users[bet[0]].username} lost ${ms} ${bet[2]}`, 'bot', { tag: global.data.users[bet[0]].username }, 1);
        await user.send(statusEmbed(`Résultats du pari "${this.name}"`, COLORS.error, "https://emojipedia-us.s3.dualstack.us-west-1.amazonaws.com/thumbs/120/twitter/281/pleading-face_1f97a.png", `Aïe ! Ton pari sur l'issue **${this.outcomes[chosenOutcome]}** t'a fait perdre ${ms} ${bet[2]}.`))
      }
    }

    this.state = Bet.ENDED;
  }

  totalForOutcome(outcome) {
    return this.bets.reduce((prev, current) => current[1] === outcome ? prev + current[2] : prev, 0);
  }

  get total() {
    return this.bets.reduce((prev, current) => current[2] + prev, 0);
  }

  get nbUsers() {
    let users = [];
    for (let bet of this.bets) {
      if (!users.includes(bet[0]))
        users.push(bet[0]);
    }
    return users.length;
  }

  static exists(id) {
    return global.data.bets.some(bet => bet.id === id);
  }

  static findByID(id) {
    return global.data.bets.find(bet => bet.id === id)
  }

  static findByName(name) {
    return global.data.bets.find(bet => bet.name.toLowerCase() === name.toLowerCase() && bet.state === Bet.OPEN)
  }

  static alreadyOpened(id) {
    return Bet.findByName(Bet.findByID(id).name) !== undefined;
  }

  async print(msg, prefix = "", displayID = false) {
    const embed = new Discord.EmbedBuilder()
      .setColor(COLORS.info)
      .setTitle(`${prefix}${this.name} ${displayID ? `(état : ${this.state === Bet.CLOSED ? "fermé" : "ouvert"})` : ""}`)
      .setDescription(`${this.nbUsers} 👥 | ${ms} ${this.total}`)
      .addFields(this.outcomes.map((outcome, index) => {
        const percent = this.total === 0 ? 0 : this.totalForOutcome(index) / this.total;
        return {
          name: `${emojiNumbers[index % 10]} → ${outcome}`,
          value: `${(percent * 100).toFixed(1)} %`,
        }
      }))
    if (displayID) embed.setFooter({ text: this.id.toString() });
    await msg.channel.send(embed);
  }
}

Bet.CLOSED = 0;
Bet.OPEN = 1;
Bet.ENDED = 2;
Bet.DELETED = 3;

module.exports = Bet;
