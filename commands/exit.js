const { Command } = require('../utils/classes');
const { saveData, statusEmbed } = require('../utils/functions');
const { COLORS } = require('../utils/constants');

let exit = new Command('exit', 'déconnecte le bot', 1, ['stop', 'quit', 'quitter', 'disconnect']);

exit.run = async function(msg) {
  await msg.channel.send(statusEmbed(
    "Bye bye",
    COLORS.result,
    "https://emojipedia-us.s3.dualstack.us-west-1.amazonaws.com/thumbs/240/google/241/waving-hand_1f44b.png",
    "Je m'en vais...\nA la prochaine !"
  ));

  await saveData();
  await global.client.destroy();
  process.exit(0);
}

module.exports = exit;
