const Discord = require('discord.js');
const { Command } = require('../utils/classes');
const Bet = require('../utils/games/bet');
let { statusEmbed, createUser, removeCommandPrefix, roundMoney } = require('../utils/functions');
const { COLORS, emojiNumbers } = require('../utils/constants');
const ms = global.registry.moneySymbol;

global.data.bets.forEach(el => Object.setPrototypeOf(el, Bet.prototype));

let bet = new Command('bet', 'permet de parier ou de gérer les paris', Command.EVERYONE, ['pari', 'parier'], '<nom du pari>; <issue>; <mise>', Command.NO_CHANNELS);
bet.dispBaseCommandInSubcommandMenu = true;
bet.baseCommandDescription = "permet de parier";

bet.subcommands["create"] = new Command('create', 'crée un nouveau pari', Command.MODS_ONLY, [], '<nom>; <issue 1>; <issue 2>; ...');
bet.subcommands["open"] = new Command('open', 'ouvre un pari au public', Command.MODS_ONLY, [], '<id>');
bet.subcommands["close"] = new Command('close', 'ferme un pari', Command.MODS_ONLY, [], '<id>');
bet.subcommands["end"] = new Command('end', 'termine un pari en précisant l\'issue gagnante', Command.MODS_ONLY, [], '<id>; <issue gagnante>');
bet.subcommands["delete"] = new Command('delete', 'supprime un pari', Command.MODS_ONLY, [], '<id>');
bet.subcommands["info"] = new Command('info', 'affiche détail d\'un pari', Command.EVERYONE, [], '<nom>');
bet.subcommands["list"] = new Command('list', 'permet de lister les paris ouverts', Command.EVERYONE);
bet.subcommands["all"] = new Command('all', 'liste tous les paris ouverts et fermés', Command.MODS_ONLY);
bet.subcommands["consult"] = new Command('consult', 'envoie en MP les paris en cours de l\'utilisateur');

bet.run = async function (msg) {
  await createUser(msg);
  logger.log(`New bet: ${msg.content}`, 'bot', msg.author, 1);
  let [name, outcome, amount] = removeCommandPrefix(msg).split(/\s*;\s*/);
  if (name === "") {
    msg.content = `${global.registry.prefix}help bet`;
    return global.registry.commands["help"].run(msg);
  }
  const bet = Bet.findByName(name);

  if (bet === undefined) {
    return msg.reply("ce pari n'est pas ouvert ou n'existe pas.");
  }

  if (bet.getOutcome(outcome) === null) {
    return msg.reply("l'issue choisie n'est pas valide.");
  }

  amount = Number(amount);
  if (isNaN(amount) || amount < 0) {
    return msg.channel.send(statusEmbed("Montant invalide !", COLORS.error));
  } else if (amount < 0.005) {
    return msg.channel.send(statusEmbed(`Tu veux parier ${ms} 0, vraiment ? Autant ne rien faire !`, COLORS.error));
  }

  amount = roundMoney(amount);
  if (global.data.users[msg.author.id].money < amount) {
    return msg.channel.send(statusEmbed("Tu n'as pas assez d'argent", COLORS.error));
  }

  if (bet.bet(msg.author.id, outcome, amount) === null) {
    return msg.channel.send(statusEmbed("Tu as déjà parié sur une issue différente !", COLORS.error));
  }

  let displayName = global.data.users[msg.author.id].nickname || global.data.users[msg.author.id].username;
  await msg.channel.send(statusEmbed(`${displayName} a parié ${ms} ${amount} sur l'issue ${outcome} !`, COLORS.success));
}

bet.subcommands["create"].run = async function (msg) {
  await createUser(msg);
  logger.log(`Bet created: ${msg.content}`, 'bot', msg.author, 1);
  const [name, ...outcomes] = removeCommandPrefix(msg).split(/\s*;\s*/);
  const forbiddenNames = Object.keys(bet.subcommands);
  if (forbiddenNames.includes(name)) {
    return msg.reply("ce nom de pari est interdit.");
  }
  const newBet = new Bet(name, outcomes);
  global.data.bets.push(newBet);
  await newBet.print(msg, "💰 Nouveau pari : ", true)
}

bet.subcommands["open"].run = async function (msg) {
  await createUser(msg);
  logger.log(`Bet opened: ${msg.content}`, 'bot', msg.author, 1);
  const id = removeCommandPrefix(msg);
  if (!Bet.exists(id)) {
    return msg.reply("ce pari n'existe pas.");
  }
  if (Bet.alreadyOpened(id)) {
    return msg.reply("un pari avec le même nom est déjà ouvert.");
  }
  const bet = Bet.findByID(id);
  if (bet.state === Bet.ENDED) {
    return msg.reply("ce pari est déjà terminé.");
  }
  if (bet.state === Bet.DELETED && msg.author.id !== global.registry.owner) {
    return msg.reply("ce pari est indisponible.");
  }
  bet.open();
  await bet.print(msg, "✅ Pari ouvert : ", true)
}

bet.subcommands["close"].run = async function (msg) {
  await createUser(msg);
  logger.log(`Bet closed: ${msg.content}`, 'bot', msg.author, 1);
  const id = removeCommandPrefix(msg);
  if (!Bet.exists(id)) {
    return msg.reply("ce pari n'existe pas.");
  }
  if (!Bet.alreadyOpened(id)) {
    return msg.reply("ce pari n'est pas ouvert.");
  }
  const bet = Bet.findByID(id);
  if (bet.state === Bet.CLOSED) {
    return msg.reply("ce pari est déjà fermé.");
  }
  if (bet.state === Bet.ENDED) {
    return msg.reply("ce pari est déjà terminé.");
  }
  if (bet.state === Bet.DELETED && msg.author.id !== global.registry.owner) {
    return msg.reply("ce pari est indisponible.");
  }
  bet.close();
  await bet.print(msg, "⛔ Pari fermé : ", true)
}

bet.subcommands["end"].run = async function (msg) {
  await createUser(msg);
  logger.log(`Bet ended: ${msg.content}`, 'bot', msg.author, 1);
  const [id, outcome] = removeCommandPrefix(msg).split(/\s*;\s*/);
  if (!Bet.exists(id)) {
    return msg.reply("ce pari n'existe pas.");
  }
  const bet = Bet.findByID(id);
  if (bet.state === Bet.ENDED) {
    return msg.reply("ce pari est déjà clos.");
  }
  if (bet.state === Bet.DELETED && msg.author.id !== global.registry.owner) {
    return msg.reply("ce pari est indisponible.");
  }
  if (bet.getOutcome(outcome) === null) {
    return msg.reply("l'issue choisie n'est pas valide.");
  }
  await bet.end(outcome);
  await bet.print(msg, "🛑 Pari terminé : ", true)
}

bet.subcommands["delete"].run = async function (msg) {
  await createUser(msg);
  logger.log(`Bet deleted: ${msg.content}`, 'bot', msg.author, 1);
  const id = removeCommandPrefix(msg);
  if (!Bet.exists(id)) {
    return msg.reply("ce pari n'existe pas.");
  }
  const bet = Bet.findByID(id);
  bet.delete();
  await bet.print(msg, "❌ Pari supprimé : ", true)
}

bet.subcommands["info"].run = async function (msg) {
  await createUser(msg);
  const name = removeCommandPrefix(msg);
  const bet = Bet.findByName(name);
  if (bet === undefined) {
    return msg.reply("ce pari n'est pas ouvert ou n'existe pas.");
  }
  bet.print(msg);
}

bet.subcommands["list"].run = async function (msg) {
  await createUser(msg);
  const openedBets = global.data.bets.filter(bet => bet.state === Bet.OPEN);
  openedBets.forEach(bet => bet.print(msg));
  if (openedBets.length === 0)
    await msg.channel.send(statusEmbed("Pas de paris ouverts pour le moment", COLORS.error));
}

bet.subcommands["all"].run = async function (msg) {
  await createUser(msg);
  const bets = global.data.bets.filter(bet => bet.state === Bet.OPEN || bet.state === Bet.CLOSED);
  bets.forEach(bet => bet.print(msg, "", true));
  if (bets.length === 0)
    await msg.channel.send(statusEmbed("Pas de paris pour le moment", COLORS.error));
}

bet.subcommands["consult"].run = async function (msg) {
  await createUser(msg);
  const bets = global.data.bets.filter(bet => (bet.state === Bet.OPEN || bet.state === Bet.CLOSED));
  const embed = new Discord.EmbedBuilder()
    .setColor(COLORS.info)
    .setTitle("Liste de tes paris");
  let empty = true;
  for (let i = 0; i < bets.length; i++) {
    const bet = bets[i].bets.find(bet => bet[0] === msg.author.id);
    if (bet !== undefined) {
      embed.addFields({
        name: bets[i].name,
        value: `${emojiNumbers[bet[1] % 10]} ${bets[i].outcomes[bet[1]]} | ${ms} ${bet[2]}`
      });
      empty = false;
    }
  }
  if (empty) embed.setDescription("Tu n'as pas de paris pour le moment !")
  await msg.author.send(embed);
}

module.exports = bet;