const { Command } = require('../utils/classes');
const { statusEmbed } = require('../utils/functions');
const { COLORS } = require('../utils/constants');
const AzGame = require('../utils/games/az');

// Buggy so disabled, but it would be great if games are stored in global.data so that the get into data.json
// client.on('ready', async () => {
//   for (let chan in AzGame.games) {
//     // The below line is only to prevent WebStorm from annoying me with a useless warning
//     if (!AzGame.games.hasOwnProperty(chan)) continue;
//
//     let channel = await client.channels.fetch(AzGame.games[chan].channel.id);
//     let azgame = new AzGame(AzGame.games[chan].wordIndex, channel);
//     Object.assign(azgame, AzGame.games[chan]);
//     azgame.channel = channel;
//   }
// });

let az = new Command('az', 'commande générale permettant de jouer à A-Zythum', 0, ['a-z', 'azythum', 'azythums', 'a-zythum', 'a-zythums']);
az.dispBaseCommandInSubcommandMenu = true;
az.baseCommandDescription = "démarre une partie avec un mot aléatoire, relance un partie en pause ou affiche l'état actuel de la partie";

az.subcommands["custom"] = new Command('custom', 'lance une partie avec un mot custom qui sera demandé en MP');
az.subcommands["pause"] = new Command('pause', 'met la partie en cours en pause');
az.subcommands["end"] = new Command('end', 'arrête la partie');

az.run = async function (msg) {
  if (msg.channel.id in AzGame.games) {
    if (AzGame.games[msg.channel.id].status == "paused") {
      await msg.channel.send(statusEmbed(
        "➣ La partie de A-Zythum a redémarré",
        COLORS.info
      ));
      AzGame.games[msg.channel.id].resume();
    }

    AzGame.games[msg.channel.id].statusMessage();
  }
  else {
    AzGame.games[msg.channel.id] = new AzGame(-1, msg.channel);
    AzGame.games[msg.channel.id].start();
  }
};

az.subcommands["custom"].run = async function (msg) {
  if (msg.channel.id in AzGame.games) {
    await msg.channel.send(statusEmbed(
      "Il y a déjà une partie de A-Zythum en cours !",
      COLORS.error
    ));
  } else {
    let newGame = new AzGame(-1, msg.channel);
    newGame.isCustom = true;
    AzGame.games[msg.channel.id] = newGame;
    AzGame.games[msg.channel.id].status = "waiting";

    msg.author.createDM()
      .then(chan => {
        chan.send("Envoie ton mot pour le faire deviner aux autres zigotos.");

        let filter = m => m.author.id == msg.author.id;
        let coll = chan.createMessageCollector(filter);

        coll.on('collect', m => {
          let mTxt = m.content.trim().toLowerCase();
          let mContent = mTxt.split(/\s+/);
          if (mContent.length == 1 && global.registry.words.indexOf(mContent[0]) != -1) {
            newGame.setWord(mContent[0]);
            newGame.start();
            chan.send("Parfait, la partie commence !");
          } else {
            chan.send("Ce n'est pas un mot valide. Retape la commande dans le salon où tu veux jouer STP.");
            newGame.end();
          }
          coll.stop();
        });
      })
      .catch(err => {
        logger.log(`Erreur lors de la création d'un chan DM pour ${msg.author.tag} : ${err.message}`, 'bot', logInternalUser, 3);
      });
  }
};

az.subcommands["pause"].run = async function (msg) {
  if (msg.channel.id in AzGame.games) {
    if (AzGame.games[msg.channel.id].status == "started") {
      AzGame.games[msg.channel.id].pause();
      await msg.channel.send(statusEmbed(
        "⨯ Partie de A-Zythum mise en pause",
        COLORS.stop
      ));
    } else if (AzGame.games[msg.channel.id].status == "paused") {
      await msg.channel.send(statusEmbed(
        "La partie est déjà en pause !",
        COLORS.error
      ));
    } else if (AzGame.games[msg.channel.id].status == "waiting") {
      await msg.channel.send(statusEmbed(
        "La partie est en préparation !",
        COLORS.error
      ));
    }
  } else {
    await msg.channel.send(statusEmbed(
      "Il n'y a aucune partie de A-Zythum en cours !",
      COLORS.error
    ));
  }
};

az.subcommands["end"].run = async function (msg) {
  if (msg.channel.id in AzGame.games) {
    if (AzGame.games[msg.channel.id].status == "waiting") {
      await msg.channel.send(statusEmbed(
        "La partie en cours de préparation a été arrêtée",
        COLORS.stop,
        "https://emojipedia-us.s3.dualstack.us-west-1.amazonaws.com/thumbs/240/google/241/stop-sign_1f6d1.png",
        `La partie de A-Zythum qui était en cours de préparation a été arrêtée par ${msg.author.username}.`
      ));
    } else {
      await msg.channel.send(statusEmbed(
        "La partie est terminée",
        COLORS.stop,
        "https://emojipedia-us.s3.dualstack.us-west-1.amazonaws.com/thumbs/240/google/241/stop-sign_1f6d1.png",
        `La partie de A-Zythum qui était en cours a été arrêtée par ${msg.author.username}.
Il y avait jusque là ${AzGame.games[msg.channel.id].tries} mots proposés.
La mot était **${global.registry.words[AzGame.games[msg.channel.id].wordIndex]}.**`
      ));
    }

    AzGame.games[msg.channel.id].end();
  } else {
    await msg.channel.send(statusEmbed(
      "Il n'y a aucune partie de A-Zythum en cours !",
      COLORS.error
    ));
  }
};

module.exports = az;