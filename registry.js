const fs = require('fs');
const config = require('./config');

global.registry = {};

global.registry.prefix = config.prefix;
global.registry.moneySymbol = config.moneySymbol;
global.registry.owner = config.owner;
global.registry.moderators = config.moderators;

global.registry.channels = config.channels.map(el => el[0]);
global.registry.words = require("./mots.json");
global.registry.coincheTxt = fs.readFileSync('coinche.txt').toString();

// Commandes
global.registry.commands = {};
let commands = fs.readdirSync('commands');

for (let command of commands) {
  let cmd = require('./commands/' + command);
  let names = [cmd.key, ...cmd.aliases];

  for (let name of names) {
    if (global.registry.commands.hasOwnProperty(name))
      throw 'duplicate command or alias';
    else global.registry.commands[name] = cmd;
  }
}