const Command = require('../utils/classes').Command;
const { statusEmbed } = require('../utils/functions');
const { COLORS, ERRORS } = require('../utils/constants');

let dice = new Command('dice', 'lance des dés', 0, ['dices', 'dé', 'dés'], '<nombre de faces> [nombre de dés]');

dice.run = async function(msg) {
  let parts = msg.content.split(/\s+/);
  if (parts.length < 2)
    return msg.channel.send(statusEmbed(ERRORS.nArgs(1), COLORS.error));

  parts[1] = parseInt(parts[1]);
  if (isNaN(parts[1]) || parts[1] <= 0 || parts[1] > 1000000)
    return msg.channel.send(statusEmbed("Nombre de faces invalide (1 000 000 max)", COLORS.error));

  if (parts.length == 2)
    parts[2] = 1;
  parts[2] = parseInt(parts[2]);
  if (isNaN(parts[2]) || parts[2] <= 0 || parts[2] > 250)
    return msg.channel.send(statusEmbed("Nombre de dés invalide (250 max)", COLORS.error));

  let result = [];
  for (let i = 0; i < parts[2]; i++) {
    result.push(Math.ceil(Math.random() * parts[1]));
  }
  let sum = result.reduce((acc, el) => acc + el);
  result = result.join(', ');

  if (result.length > 5000)
    return msg.channel.send(statusEmbed("Du calme ! Tu as lancé tellement de dés que je ne peux pas afficher le résultat :(", COLORS.error));

  let embed = statusEmbed(`Résultat du lancer de \`${parts[2]}\` dé${parts[2] > 1 ? 's' : ''} \`${parts[1]}\``, COLORS.status, "https://emojipedia-us.s3.dualstack.us-west-1.amazonaws.com/thumbs/240/twitter/281/game-die_1f3b2.png", result);
  if (parts[2] > 1) embed.addFields({
    name: "Somme des valeurs obtenues",
    value:sum
  });
  return msg.channel.send(embed);
}

module.exports = dice;