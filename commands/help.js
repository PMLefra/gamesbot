const { Command } = require('../utils/classes');
const { frsort, statusEmbed, capitalize } = require('../utils/functions');
const { COLORS } = require('../utils/constants');
const prefix = global.registry.prefix;

const spaceAndArrow = '\u00a0\u00a0→ ';

let help = new Command('help', 'affiche le menu d\'aide', Command.EVERYONE, ['aide'], '[commande]');

help.run = async function(msg) {
  let commandKeys = Object.keys(global.registry.commands)
    .filter(str => global.registry.commands[str].key == str)
    .filter(str => global.registry.commands[str].userAllowed(msg.author))
    .filter(str => global.registry.commands[str].channelAllowed(msg.channel))
    .sort(frsort);

  let parts = msg.content.split(/\s+/);

  // Seeking help for a specific command
  if (parts.length > 1 && commandKeys.includes(parts[1])) {
    let commandName = parts[1].toLowerCase();
    let command = global.registry.commands[commandName];

    let responseContent = command.help.length > 0 ? command.help : command.description;
    responseContent = capitalize(responseContent);

    // Aliases
    if (command.aliases.length > 0)
      responseContent += `\n${spaceAndArrow}Alias : ${command.aliases.map(el => '`' + el + '`').join(", ")}`;

    // Usage
    if (command.usage.length > 0 && !command.dispBaseCommandInSubcommandMenu)
      responseContent += `\n${spaceAndArrow}Usage : \`${prefix}${command.key} ${command.usage}\``;

    /*
      SUBCOMMANDS
    */

    responseContent += '\n';

    /* Special case for the base command, if we want it displayed in this
    subcommands section */
    if (command.dispBaseCommandInSubcommandMenu) {
      let descText = command.baseCommandDescription.length > 0 ? command.baseCommandDescription : command.description;
      responseContent += `\n**${prefix}${commandName}** : ${descText}`;
      if (command.usage.length > 0 && command.dispSubcommandUsage)
        responseContent += `\n${spaceAndArrow}Usage : \`${prefix}${commandName} ${command.usage}\``;
    }

    // Regular subcommands
    if (Object.keys(command.subcommands).length > 0) {
      let subcommandKeys = Object.keys(command.subcommands)
        .filter(str => command.subcommands[str].userAllowed(msg.author))
        .filter(str => command.subcommands[str].channelAllowed(msg.channel))
        .sort(frsort);

      for (let subcommandName of subcommandKeys) {
        let subcommand = command.subcommands[subcommandName];

        responseContent += `\n**${prefix}${commandName} ${subcommandName}** : ${subcommand.description}`;
        if (subcommand.usage.length > 0 && command.dispSubcommandUsage)
          responseContent += `\n${spaceAndArrow}Usage : \`${prefix}${commandName} ${subcommandName} ${subcommand.usage}\``;
      }
    }

    await msg.channel.send(statusEmbed(
      `Commande \`${parts[1]}\` :`,
      COLORS.statusDark,
      "https://emojipedia-us.s3.dualstack.us-west-1.amazonaws.com/thumbs/240/twitter/281/speech-balloon_1f4ac.png",
      responseContent.trim()
    ));

    return;
  }

  // Seeking general help
  let commandList = `Tapez **${prefix}help *commande*** pour avoir des informations complémentaires sur une commande\n\n`;
  for (let cmdName of commandKeys) {
    let cmd = global.registry.commands[cmdName];
    commandList += `**${prefix}${cmdName}** : ${cmd.description}\n`;
  }
  await msg.channel.send(statusEmbed(
    "Liste des commandes",
    COLORS.statusDark,
    "https://emojipedia-us.s3.dualstack.us-west-1.amazonaws.com/thumbs/240/google/241/thinking-face_1f914.png",
    commandList
  ));
}

module.exports = help;