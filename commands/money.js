const Discord = require('discord.js');
const { Command } = require('../utils/classes');
let { statusEmbed, createUser, getUser, setMoney, addMoney } = require('../utils/functions');
const { COLORS, ERRORS } = require('../utils/constants');
const ms = global.registry.moneySymbol;

let money = new Command('money', "permet de consulter ou d'effectuer des actions sur votre solde ou celle d'un autre joueur", 0, ['cash', 'argent'], '[joueur]');
money.dispBaseCommandInSubcommandMenu = true;
money.baseCommandDescription = "fait la même chose que `$money balance`";

money.subcommands["balance"] = new Command('balance', "consulte votre solde ou celle d'un autre joueur", Command.EVERYONE, [], '[joueur]');
money.subcommands["give"] = new Command('give', "donne de l'argent à un joueur", Command.EVERYONE, [], '<joueur> <montant>');
money.subcommands["add"] = new Command('add', "ajoute une somme d'argent à la balance d'un joueur", Command.MODS_ONLY, [], '<joueur> <montant>');
money.subcommands["set"] = new Command('set', "définit la quantité d'argent d'un joueur", Command.MODS_ONLY, [], '<joueur> <montant>');
money.subcommands["top"] = new Command('top', "affiche le classement du solde des joueurs", Command.EVERYONE, [], '[nombre]');

async function checkBalance(msg) {
  let args = msg.content.split(/\s+/);

  if (args.length < 2) {
    await createUser(msg);

    let cash = global.data.users[msg.author.id].money;
    let displayName = global.data.users[msg.author.id].nickname || global.data.users[msg.author.id].username;
    await msg.channel.send(statusEmbed(`Argent de ${displayName}`, COLORS.status, "https://emojipedia-us.s3.dualstack.us-west-1.amazonaws.com/thumbs/240/twitter/281/money-bag_1f4b0.png", `${ms} **${cash}**`));
  } else {
    let id = getUser(args[1]);

    if (id == -1)
      await msg.channel.send(statusEmbed("Cet utilisateur n'existe pas ou n'a pas ouvert de compte", COLORS.error));
    else {
      let user = global.data.users[id];
      let displayName = global.data.users[id].nickname || global.data.users[id].username;
      await msg.channel.send(statusEmbed(`Argent de ${displayName}`, COLORS.status, "https://emojipedia-us.s3.dualstack.us-west-1.amazonaws.com/thumbs/240/twitter/281/money-bag_1f4b0.png", `${ms} **${user.money}**`));
    }
  }
}

money.run = checkBalance;
money.subcommands["balance"].run = checkBalance;

money.subcommands["give"].run = async function (msg) {
  await createUser(msg);

  let args = msg.content.split(/\s+/);
  if (args.length < 3)
    return msg.channel.send(statusEmbed(ERRORS.nArgs(2), COLORS.error));

  let userID = getUser(args[1]);
  if (userID == -1)
    return msg.channel.send(statusEmbed(`L'utilisateur '${args[1]}' n'existe pas ou n'a pas ouvert de compte`, COLORS.error));

  let amount = Number(args[2]);
  if (isNaN(amount))
    return msg.channel.send(statusEmbed("Le deuxième argument donné n'est pas un nombre valable", COLORS.error));

  let precision = (amount.toString().indexOf(".") != -1) ? (amount.toString()).split(".")[1].length : 0;
  if (amount < 0.01 || precision > 2)
    return msg.channel.send(statusEmbed(`La somme \`${amount}\` n'est pas valide`, COLORS.error));

  if (global.data.users[msg.author.id].money < amount)
    return msg.channel.send(statusEmbed("Tu n'as pas assez d'argent", COLORS.error));

  addMoney(msg.author.id, -amount);
  addMoney(userID, amount);

  await msg.channel.send(statusEmbed(`${msg.author.username}, tu as bien donné ${ms} ${amount} à ${global.data.users[userFID].username}`, COLORS.success));
}

money.subcommands["add"].run = async function (msg) {
  await createUser(msg);

  let args = msg.content.split(/\s+/);
  if (args.length < 3)
    return msg.channel.send(statusEmbed(ERRORS.nArgs(2), COLORS.error));

  let userID = getUser(args[1]);
  if (userID == -1)
    return msg.channel.send(statusEmbed(`L'utilisateur '${args[1]}' n'existe pas ou n'a pas ouvert de compte`, COLORS.error));

  let amount = Number(args[2]);
  if (isNaN(amount))
    return msg.channel.send(statusEmbed("Le deuxième argument donné n'est pas un nombre valable", COLORS.error));

  let precision = (amount.toString().indexOf(".") != -1) ? (amount.toString()).split(".")[1].length : 0;
  if (precision > 2)
    return msg.channel.send(statusEmbed(`La somme \`${amount}\` n'est pas valide`, COLORS.error));

  addMoney(userID, amount);
  let displayName = global.data.users[userID].nickname || global.data.users[userID].username;
  logger.log(`${ms} ${amount} créé(s) pour ${userID}`, 'bot', msg.author, 1);
  await msg.channel.send(statusEmbed(`${displayName} a désormais ${ms} ${global.data.users[userID].money}`, COLORS.success));
}

money.subcommands["set"].run = async function (msg) {
  await createUser(msg);

  let args = msg.content.split(/\s+/);
  if (args.length < 3)
    return msg.channel.send(statusEmbed(ERRORS.nArgs(2), COLORS.error));

  let userID = getUser(args[1]);
  if (userID == -1)
    return msg.channel.send(statusEmbed(`L'utilisateur '${args[1]}' n'existe pas ou n'a pas ouvert de compte`, COLORS.error));

  let amount = Number(args[2]);
  if (isNaN(amount))
    return msg.channel.send(statusEmbed("Le deuxième argument donné n'est pas un nombre valable", COLORS.error));

  let precision = (amount.toString().indexOf(".") != -1) ? (amount.toString()).split(".")[1].length : 0;
  if (precision > 2)
    return msg.channel.send(statusEmbed(`La somme \`${amount}\` n'est pas valide`, COLORS.error));

  setMoney(userID, amount);
  let displayName = global.data.users[userID].nickname || global.data.users[userID].username;
  logger.log(`Argent de ${userID} défini à ${ms} ${amount}`, 'bot', msg.author, 1);
  await msg.channel.send(statusEmbed(`${displayName} a désormais ${ms} ${global.data.users[userID].money}`, COLORS.success));
}

money.subcommands["top"].run = async function (msg) {
  let args = msg.content.split(/\s+/);
  let amount = parseInt(args[1]);
  if (amount < 1 || isNaN(amount)) amount = 10;
  const embed = new Discord.EmbedBuilder()
    .setColor(COLORS.info)
    .setTitle("🏆 Classement général 🏆")
    .setDescription(`Top ${amount} 👥`)
    .addFields(Object.values(global.data.users)
      .sort((a, b) => a.money - b.money)
      .slice(-amount)
      .reverse()
      .map((user, index) => ({ name: `${index + 1}. ${user.nickname || user.username}`, value: `${ms} ${user.money}` }))
    )

  await msg.channel.send({embeds: [embed]});
}

module.exports = money;
