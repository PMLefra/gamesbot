const { statusEmbed, createUser, addMoney, removeAccents } = require("../functions");
const { COLORS } = require('../constants');
const Logger = require("js-logger");

class Hint {
  constructor(word, game) {
    this.word = word;
    this.result = "";

    let realWord = game.word.split("");
    let guessWord = word.split("");

    // The first loop checks for correct letters at the correct spot
    for (let i = 0; i < guessWord.length; i++) {
      if (guessWord[i] === realWord[i]) {
        game.correctLetters[i] = guessWord[i].toUpperCase();
        guessWord[i] = 1;
        realWord[i] = 1;
      }
    }

    // The second loop checks for correct letters at the wrong spot
    for (let i = 0; i < guessWord.length; i++) {
      if (guessWord[i] === 1) continue;

      let index = realWord.indexOf(guessWord[i]);
      if (index !== -1) {
        realWord[index] = 0;
        guessWord[i] = 0;
      } else guessWord[i] = -1;
    }

    // Updating possible letters
    let impossibleLetters = [];
    for (let i = 0; i < word.length; i++) {
      if (guessWord[i] === -1) {
        let canPush = true;
        for (let j = 0; j < word.length; j++) {
          if (word[i] !== word[j]) continue;
          if (guessWord[j] !== -1) canPush = false;
        }
        if (canPush) impossibleLetters.push(word[i]);
      }
    }
    game.possibleLetters = game.possibleLetters.filter(el => !impossibleLetters.includes(el));

    this.result = guessWord.map(el => {
      switch (el) {
        case -1: return "⬛";
        case  0: return"🟡";
        case 1: return"🟩";
      }
    }).join("");
  }
}

class WordleGame {
  static dictionary = global.registry.words
    .filter(word => word.length == 5 && !word.includes("-") && !word.includes("'"))
    .map(word => removeAccents(word));

  static games = {};
  
  constructor(wordIndex, channel) {
    this.wordIndex = wordIndex == -1 ? Math.floor(Math.random() * WordleGame.dictionary.length) : wordIndex;
    this.word = WordleGame.dictionary[this.wordIndex];
    this.hints = [];
    this.channel = channel;
    this.status = "not-started";
    this.tries = 0;
    this.possibleLetters = ["a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"];
    this.correctLetters = ["-", "-", "-", "-", "-"];
    this.isCustom = false;

    this.messageListener = async msg => {
      if (msg.channel.id != this.channel.id) return;
      if (msg.author.id == global.client.user.id) return;
      if (this.status != "started") return;

      await this.guess(msg);
    }

    global.client.on("messageCreate", this.messageListener);
  }

  setWord(newWord) {
    let newIndex = WordleGame.dictionary.indexOf(newWord.toLowerCase());
    if (newIndex != -1) {
      this.wordIndex = newIndex;
      this.word = WordleGame.dictionary[this.wordIndex];
    }
    this.hints = [];
  }

  start() {
    this.status = "started";
    if (!WordleGame.games.hasOwnProperty(this.channel.id))
      WordleGame.games[this.channel.id] = this;

    this.channel.send(statusEmbed(
      "C'est parti !",
      COLORS.info,
      "https://emojipedia-us.s3.dualstack.us-west-1.amazonaws.com/thumbs/240/google/241/chequered-flag_1f3c1.png",
      "Une partie de Wordle vient de démarrer.\nTrouve le mot de 5 lettres auquel je pense en proposant des mots et en tenant compte des indices. Attention, tu n'as que 6 essais !"
    ));
  }

  pause() {
    this.status = "paused";
  }

  resume() {
    this.status = "started";
  }

  #generateHintText() {
    let fstr = "";
    for (let hint of this.hints) {
      fstr += `${hint.result} — **${hint.word.toUpperCase()}**\n`;
    }
    return fstr.trim();
  }

  statusMessage() {
    if (this.status == "paused") this.channel.send(statusEmbed(
      "La partie est en pause",
      COLORS.statusDark,
      "https://emojipedia-us.s3.dualstack.us-west-1.amazonaws.com/thumbs/240/twitter/281/hourglass-done_231b.png",
      ""
    ));
    else if (this.status == "waiting") this.channel.send(statusEmbed(
      "🤷‍♂️ Une partie est en cours de préparation",
      COLORS.warning
    ));

    if (this.status != "started") return;

    this.channel.send(statusEmbed(
      "État de la partie",
      COLORS.status,
      "", this.#generateHintText(),
      `${this.tries} essai${this.tries > 1 ? 's' : ''}` +
      `\nLettres correctes et bien placées : ${this.correctLetters.join("")}` +
      `\nLettres possibles : ${this.possibleLetters.join(", ").toUpperCase()}`
    ));
  }

  async guess(msg) {
    if (this.status != "started") return;
    if (msg.content.length > 1 && msg.content[0] == global.registry.prefix) return;

    let msgTxt = msg.content.trim();
    if (msgTxt.split(/\s+/).length != 1) return;
    msgTxt = removeAccents(msgTxt.toLowerCase());

    if (!WordleGame.dictionary.includes(msgTxt)) msg.react('❌').catch();
    else {
      this.tries++;
      this.hints.push(new Hint(msgTxt, this));
      if (msgTxt == this.word) {
        await this.win(msg);
      } else {
        if (this.tries < 6) this.statusMessage();
        else this.lose(msg);
      }
    }
  }

  async win(msg) {
    let totalTries = this.tries;
    let responseContent = `${msg.author.username} a trouvé le bon mot après ${totalTries} proposition${totalTries > 1 ? 's' : ' (seulement 😮)'}, qui était **${this.word.toUpperCase()}**.\n`;
    responseContent += this.#generateHintText();

    if (!this.isCustom) {
      await createUser(msg).catch(e => logger.log(e.toString(), "create-user", Logger.INTERNAL, 3));
      addMoney(msg.author.id, 7);
      responseContent += `\nIncroyable ! ${global.registry.moneySymbol} 7 sont apparus sur son compte, on l'applaudit tous, s'il vous plaît.`;
    } else
      responseContent += `\nPuisqu'il s'agissait d'une partie custom, iel ne gagne aucun ${global.registry.moneySymbol}, mais on l'applaudit tous quand même, s'il vous plaît.`;

    this.channel.send(statusEmbed(
      "Félicitations !",
      COLORS.success,
      "https://emojipedia-us.s3.dualstack.us-west-1.amazonaws.com/thumbs/240/google/241/party-popper_1f389.png",
      responseContent
    ));

    this.end();
  }

  lose() {
    let responseContent = "Mince alors, vous n'avez plus de tentative restante pour trouver le mot. Voici ce que vous aviez pour l'instant :\n";
    responseContent += this.#generateHintText();
    responseContent += `\nLe mot était **${this.word.toUpperCase()}**.\nLa partie est terminée !`;

    this.channel.send(statusEmbed(
      "Aïe !",
      COLORS.statusDark,
      "https://emojipedia-us.s3.dualstack.us-west-1.amazonaws.com/thumbs/240/twitter/282/pensive-face_1f614.png",
      responseContent
    ));

    this.end();
  }

  end() {
    this.status = "ended";
    global.client.removeListener("messageCreate", this.messageListener);
    delete WordleGame.games[this.channel.id];
  }
}


module.exports = WordleGame;