const Discord = require('discord.js');
const fs = require('fs');
const { userRegex, COLORS } = require('./constants');

function capitalize(string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
}

function statusEmbed(msg = "Vide", color = COLORS.result, pic = "", content = "", footerTxt = "", footerImg = "") {
  let embed = new Discord.EmbedBuilder()
    .setColor(color)
    .setTitle(msg)

  if (content.length > 0) embed = embed.setDescription(content);
  if (pic.length > 0) embed = embed.setThumbnail(pic);

  const footer = {};
  if (footerTxt.length > 0) footer.text = footerTxt;
  if (footerImg.length > 0) footer.iconURL = footerImg;
  if (Object.keys(footer).length > 0) embed = embed.setFooter(footer);

  return { embeds: [embed] };
}

function frsort(a, b) {
  return a.localeCompare(b);
}

async function createUser(msg) {
  if (!global.data.users.hasOwnProperty(msg.author.id)) {
    const member = await msg.guild.member(msg.author);
    global.data.users[msg.author.id] = {
      username: msg.author.username,
      nickname: member && member.nickname,
      money: 100,
      isBot: msg.author.bot
    };

    logger.log(`Account created`, 'bot', msg.author, 1);
    await msg.author.send(statusEmbed(
      `Ton compte est ouvert !`,
      COLORS.info,
      "https://emojipedia-us.s3.dualstack.us-west-1.amazonaws.com/thumbs/120/twitter/281/waving-hand_1f44b.png",
      `Je t'offre ${global.registry.moneySymbol} 100 pour commencer, tu peux consulter ton solde à tout moment dans un salon où je suis présent avec la commande \`$money\`.`
    ));
  }
}

function getUser(nameOrID) {
  if (userRegex.test(nameOrID))
    nameOrID = userRegex.exec(nameOrID)[1];

  if (nameOrID in global.data.users) return nameOrID;

  let ids = Object.keys(global.data.users);
  nameOrID = nameOrID.toLowerCase();
  for (let id of ids) {
    if (global.data.users[id].username.toLowerCase() == nameOrID
      || (global.data.users[id].nickname && global.data.users[id].nickname.toLowerCase() == nameOrID))
      return id;
  }

  return -1;
}

function setMoney(userNameOrID, amount) {
  let user = getUser(userNameOrID);
  if (user == -1) return -1;

  user = global.data.users[user];
  user.money = roundMoney(amount);
}

function addMoney(userNameOrID, amount) {
  let user = getUser(userNameOrID);
  if (user == -1) return -1;

  user = global.data.users[user];
  user.money = roundMoney(user.money + amount);
}

function roundMoney(amount) {
  return Math.round(amount * 100) / 100;
}

async function saveData() {
  return fs.promises.writeFile("data.json", JSON.stringify(global.data, null, 2), "utf-8")
    .then(() => {
      logger.log(`Data saved to data.json`, 'i/o', logInternalUser, 0);
    })
    .catch(err => {
      logger.log(`Error while saving data to disk: ${err.message}`, "i/o", logInternalUser, 4);
      process.exit(err.errno);
    });
}

async function saveWords() {
  return fs.promises.writeFile("mots.json", JSON.stringify(global.registry.words), "utf-8")
    .then(() => {
      logger.log(`Word list saved to mots.json`, 'i/o', logInternalUser, 1);
    })
    .catch(err => {
      logger.log(`Error while saving data to disk: ${err.message}`, "i/o", logInternalUser, 4);
      process.exit(err.errno);
    });
}

async function sendDM(id, msg) {
  try {
    let user = await client.users.fetch(id);
    let dmChan = await user.createDM();
    if (dmChan)
      dmChan.send(msg)
        .then(() => logger.log(msg, "dm-sent", user, 1));
  } catch (e) {
    console.error(e);
  }
}

function removeCommandPrefix(msg) {
  const parts = msg.content.split(/\s+/);
  parts.shift();
  return parts.join(" ");
}

function removeAccents(string) {
  return string.normalize("NFD").replace(/[\u0300-\u036f]/g, "");
}

module.exports = {
  capitalize,
  statusEmbed,
  frsort,
  createUser,
  addMoney,
  setMoney,
  roundMoney,
  saveData,
  saveWords,
  sendDM,
  getUser,
  removeCommandPrefix,
  removeAccents
}
