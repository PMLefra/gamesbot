#!/usr/bin/bash

cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null || exit 1

until (node . &> logs/lastlog.txt); do
  GB_EXIT_CODE=$?
	CRASH_FILE_NAME="crash-$(date '+%d-%m-%Y-%H-%M-%S')"
	mkdir -p crashes
	cp logs/lastlog.txt "crashes/$CRASH_FILE_NAME"
	echo "GamesBot crashed with exit code $GB_EXIT_CODE. Respawning..." >> "crashes/$CRASH_FILE_NAME"
	sleep 2
done
