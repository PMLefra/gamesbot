# TODO

- Centralize error messages [ Nearly done ]
- Add a help menu for subcommands
- Fix: @mentions not displayed properly in embed error messages - see $money @someoneThatDoesntHaveAnAccount
- A channel with a constantly updated message that lists the open bets
- $az hint (maybe something like "jav___r__t" if the word is "javascript"?)
- $az: earn a little money on reducing the word gap a lot (like > 90%)
- $az custom: let the game initiator put any amount of money he wants in the game so that he technically gives it to the winner  
