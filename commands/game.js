const { Command } = require('../utils/classes');
const { statusEmbed } = require('../utils/functions');
const { COLORS } = require('../utils/constants');

let game = new Command('game', "connaissez-vous le jeu ?", Command.EVERYONE, ['jeu']);

game.run = async function(msg) {
  return msg.channel.send(statusEmbed("J'ai perdu !", COLORS.info));
}

module.exports = game;