const { Command } = require('../utils/classes');
const { statusEmbed } = require('../utils/functions');
const { COLORS } = require('../utils/constants');

let eval_ = new Command('eval', 'évaluer du code JS', 2, ['évaluer'], '<code>');

eval_.run = async function(msg) {
  let parts = msg.content.split(/\s+/);
  let toEval = parts.slice(1, parts.length).join(" ");
  let res, totalTime, startTime = new Date();
  try {
    res = eval(toEval);
  } catch (e) {
    res = e;
  } finally {
    totalTime = new Date() - startTime;

    if (typeof res == 'undefined') res = 'undefined';
    else if (typeof res == 'object' && !(res instanceof Error)) res = JSON.stringify(res, null, 2);
    else res = res.toString();

    if (res.length == 0) res = '<empty>';
    if (res.length > 2000) res = res.slice(0, 2000) + '...';
    res = '```js\n' + res + '```';
  }

  await msg.channel.send(statusEmbed(
    "Voilà le résultat, boss !",
    COLORS.info,
    "",
    res,
    `Temps écoulé : ${totalTime}ms`
  ));
}

module.exports = eval_;
