const { Command } = require('../utils/classes');
const coincheTxt = global.registry.coincheTxt;

let coinche = new Command('coinche', 'affiche un giga poing');

coinche.run = async function(msg) {
  await msg.channel.send("```\n" + coincheTxt + "```");
}

module.exports = coinche;