# GamesBot

A Discord bot that allows users to play various games (in French), using its own framework.  
Install dependencies with `npm install`, then run it with node.  
You can use the `launch.sh` bash script that will keep the bot running if it crashes (e.g. `gamesbot/launch.sh &!`).

## Needed files
- `config.json`:
  ```json
  {
    "prefix": "<The bot's prefix>",
    "moneySymbol": "<Symbol for the bot currency>",
    "owner": "<ID of owner's discord account>",
    "moderators": ["<Discord account IDs list>"],
    "channels": [
      ["<Discord channel ID>", "Channel description"],
      ["<Other channel ID>", "Other description"],
      ...
    ]
  }
  ```
- `data.json`:
  ```json
  {
    "bets": [],
    "proposedwords": [],
    "users": {}
  }
  ```
- `bot.key`: Discord bot API key
